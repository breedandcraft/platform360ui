import Bb from 'backbone';
import formInterface from '../utils/backbone-form-interface-orion';
import 'imports-loader?$=jquery!../utils/backbone-forms';

export default function (options) {

	let schema = options.schema || options.model;

	Object.keys(schema).forEach((key) => {
		let field = schema[key];

		if (field.type && formInterface[field.type]) {
			Object.assign(field, formInterface[field.type]);
		}
	});

	return new Bb.Form(options);
}
