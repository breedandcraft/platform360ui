var formatsCollection = require('./formatsCollection');
var assetsCollection = require('./assetsCollection');
var creativeModel = require('./creativeModel');
var formInterface = require('../../utils/backbone-form-interface');
// gives us Backbone.Form, this may be an inefficent way to include
require('../../utils/backbone-forms');

module.exports = Backbone.Marionette.LayoutView.extend({

	regions: {
		name: '#creativeName',
		format: '#creativeFormat',
		message: '.message' 
	},
	
	template: require('./mkCreativeTemplate'),

	// format change needs to listen to the children events in the 'name' region
	events: {
		'change #mkCreativeNav input': 'formatChange',
		'click #mkCreative': 'mkCreative',
		'click .cancel': 'cancelCreative'
	},

	model: new creativeModel(),

	nameFormatProps: {
		schema: {
			Name: { 
				type: 'Text',
				placeholder: 'E.g. Tesco Banner',
				validators: [{ 
						type: 'required',
						message: 'required.'
				}]
			},
			Format: {
				type: 'Radio',
				options: {}
			}
		},
		model: {},
		initModel: {}
	},

	formFormatProps: {
		schema: {},
		initSchema: {
			width: {
				validators: [{ 
					type: 'required',
					message: 'Required.'
				}]
			},
			height: {
				validators: [{ 
					type: 'required',
					message: 'Required.'
				}]
			},
			videoDuration: {
				validators: [{ 
					type: 'range',
					max: 9999999,
					min: 1,
					message: 'Required.'
				}]
			},
			convertSeconds: {
				validators: [{
					type: 'range',
					max: 9999999,
					min: 1,
					message: 'Required.'
				}]
			}
		},
		model: {},
		initModel: {
			poster: '',
			video: '',
			image: ''
		}
	},

	formTabs: new formatsCollection(),

	imagesCollection: new assetsCollection({
		limit: -1,
		type: 'image'
	}),

	videosCollection: new assetsCollection({
		limit: -1,
		type: 'video'
	}),

	currentFormat: null,

	setFormat: function(format) {
		var self = this;

		if (this.currentFormat === format) {
			// we are already using that format, return
			return;
		} else {
			// if setFormat is called externally, update this.currentFormat, reset format schema and model
			this.currentFormat = format;
			this.formFormatProps.schema = {};
			this.formFormatProps.model = {};
		}

		//
		// find the relevent model in the collection
		//
		var formatModel = this.formTabs.find(function(model) {
			return model.get("name") === format;
		});
		
		//
		// Loop the fields and build the formatForm schema
		//
		var templateFields = formatModel.get('templateFields');
		Object.keys(templateFields).forEach(function(key){
			var field =	templateFields[key];
			var fieldAttr = {};
			var fieldOptions = formInterface[field.type].options || [];

			//
			// this is a shit way of setting the default value to on/1 for a 'switch',
			// the value should be sent from the backend ideally, this could perhaps be located
			// to where we are storing static form validation
			//
			if (field.type === 'switch' || field.type === 'timeTracker') {
				self.formFormatProps.model[key] = 1;
			}

			//
			// if the field does not exist on the model, 
			// create it (nessesary for submission/validation),
			// use the 'initModel' if exists to set default values
			//
			if (self.formFormatProps.model[key] === undefined) {
				self.formFormatProps.model[key] = self.formFormatProps.initModel[key];
			}

			if (key === 'poster' || key === 'image') {
				//
				// Look inside the first item of the 'images' 'collection' because its not written
				// like a collection and the api returns an array of 'items'
				//
				self.imagesCollection.models[0].get('items').forEach(function(item) {
					fieldOptions.push({ val: item.id, label: item.name });	
				});
			}

			if (key === 'video') {
				//
				// Look inside the first item of the 'videos' 'collection' because its not written
				// like a collection and the api returns an array of 'items'
				//
				self.videosCollection.models[0].get('items').forEach(function(item) {
					fieldOptions.push({ val: item.id, label: item.name });	
				});
			}
			
			//
			// compile the final form schema using the form interface mapping object,
			// and the initial schema property (a way of setting static validation)
			//
			self.formFormatProps.schema[key] = Object.assign({},
				formInterface[field.type],
				self.formFormatProps.initSchema[key],
				{
					title: field.name,
					description: field.description,
					options: fieldOptions
				}
			);
		});

		this.renderForms();
	},

	initialize: function(options) {
		var self = this;

		Promise.all([this.formTabs.fetch(), this.videosCollection.fetch(), this.imagesCollection.fetch()]).then(function(result){
			var formTabs = result[0];

			//
			// The form tabs collection should contain the field definitions for the tab
			// as well as its name
			//
			formTabs.forEach(function (item) {
				var format = item.name;
				self.nameFormatProps.schema.Format.options[format] = item.name;
			});

			// set the current format to the first form tab recived
			self.setFormat(formTabs[0].name);

		}, function(reason) {
			console.log('Failed to fetch form assets.');
		})
	},

	/*
	 * take the schema objects, construct a backbone view/model out of them and display
	 */
    renderForms: function() {
		//
		// only re-render the top (name/format) form if it is not displayed currently
		//
		if (!this.name.hasView()) {
			this.name.show(new Backbone.Form({
				schema: this.nameFormatProps.schema,
				template: require('./nameTypeTemplate'),
				model: new Backbone.Model(
					Object.assign({},
						this.nameFormatProps.model,
						{	
							Format: this.currentFormat
						}
					)
				)
			}));
		}

		this.format.show(new Backbone.Form({ 
			schema: this.formFormatProps.schema,
			model: new Backbone.Model(this.formFormatProps.model)
		}));
	},

	formatChange: function(evt) {
		var newFormat = evt.target.value;

		if (newFormat) {
			this.setFormat(newFormat);
		}	
	},

	/*
	 * Handels form submission
	 */
	mkCreative: function(evt) {
		// maintain the formData object for submission
		var formNameData = {};
		var	formFormatData = {}; 
		var errors = {};
		var self = this;
			
		//
		// commit both forms if available and build form data
		//
		if (this.name.hasView()) {
			Object.assign(errors, this.name.currentView.commit({ validate: true }) || {});
			formNameData = this.name.currentView.model.toJSON();
		}

		if (this.format.hasView()) {
			Object.assign(errors, this.format.currentView.commit({ validate: true }) || {});
			formFormatData = this.format.currentView.model.toJSON();
		}

		//
		// if there are errors
		//
		if (Object.keys(errors).length) {
			this.showFormMessage('The form below contained errors when you tried to save. Please amend the parts highlighted in red before attempting to save again.');
			return false;
		} else {
			//
			// save the data
			//
			return this.model.save(
				{
					name: formNameData.Name,
					format: formNameData.Format,
					templateValues: formFormatData
				},
				{
					success: function(model, resp, option) {
						self.savedCreative(resp.id);
					},
					error: function(model, resp, option) {
						//
						// This is not displaying and I don't know why,
						// if I pause the debugger here, I do not appear to get a response from the server....
						//
						// If I don't pause the debugger here, I do get a response, it makes no sense...
						//
						// theorecially, below should work, its not exactly rocket science just time consuming front end BS
						//
						self.showFormMessage(resp.error);
					}
				}
			);
		}
	},

	/*
	 * show global form messages
	 * typical usage for errors
	 */
	showFormMessage: function(message, el, className) {
		var messageView = Backbone.Marionette.ItemView.extend({
			template: false,
			el: el || 'p',
			className: className || 'well decorate stop',
			onRender: function() {
				this.$el.append(message);
			}
		});
		this.message.show(new messageView());
	},

	/*
	 * on cancel creation, just go back
	 */
	cancelCreative: (evt) => {
		window.history.back();
	},

	/*
	 * on save success, attempt to trigger 'gotocreative'
	 * if this listener does not exist, override this function
	 */
	savedCreative: (creativeId) => {
		application.vent.trigger('gotoCreative', creativeId);
	}

});
