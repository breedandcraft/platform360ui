var config = require('config/config');

module.exports = Backbone.Model.extend({
	url: config.serve_api_url + '/assets'
});
