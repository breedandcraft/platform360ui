var config = require('config/config');

module.exports = Backbone.Collection.extend({
    model : require('./assetModel'),
    url : config.serve_api_url + '/assets',
	initialize: function(options) {
		if (options) {
			this.url += '?limit' + options.limit + '&type=' + options.type;
		}
	}
});
