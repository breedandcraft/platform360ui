var config = require('config/config');

module.exports = Backbone.Collection.extend({
    model: require('./formatModel'),
    url: config.serve_api_url + '/formats'
});
