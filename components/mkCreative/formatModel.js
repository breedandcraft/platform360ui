var config = require('config/config');

module.exports = Backbone.Model.extend({
  url: config.serve_api_url + '/formats',
  embedOptions: function() {

    switch (this.get("embedAs")) {
      
      case "1":
        return("This format can only be embedded with an iframe.");
      case "2":
        return("This format can only be embedded with JS.");
      case "3":
        return("This format can be embedded with an iframe or JS.");
      default:
        return("This format can't be embedded.");

    }

  }
});
