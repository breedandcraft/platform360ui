module.exports = Backbone.Marionette.ItemView.extend({

	template: require('./promptTemplate'),

	className: 'display-table modal-wrap',

	events: {
		'click .affirmative': 'affirmative',
		'click .negative': 'negative'
	},

	affirmative: function() {

		// if an action exists, fire it
		this.action(this.model.get('affirmativeAction'));

	},

	negative: function() {

		// if an action exists, fire it
		this.action(this.model.get('negativeAction'));
	},

	action: function(action) {
		var promises;

		//
		// if an ection exists, execute it
		//
		if (action) {
			promises = action();
		}

		//
		// if the action returned a promise, wait of it before emptying modal
		//
		if (promise) {
			Promise.all(promises).then(function(result) {
				application.modal.empty();
			});
		} else {
			application.modal.empty();
		}
	}

});
