module.exports = Backbone.Marionette.LayoutView.extend({

	template: require('./modalTemplate'),
	
	regions: {
		'priContent': Backbone.Maionette.Region.extend({
			el: '#modalContent',

			onShow: function() {
				debugger;
				this.$el.show();
			},

			onEmpty: function() {
				debugger;
				this.$el.hide();
			}
		})
	},

	className: 'display-table modal-wrap',

	childEvents: {
		'close:modal': function(childView) {
			debugger;
		}
	}

});


