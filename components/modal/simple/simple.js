module.exports = Backbone.Marionette.ItemView.extend({

	template: require('./simpleTemplate'),

	className: 'content content-slim overflow-h display-ib',

	events: {
		'click .close': 'close'
	},

	close: function() {
		application.modal.empty();
	}

});
