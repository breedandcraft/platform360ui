module.exports = Backbone.Marionette.ItemView.extend({

	template: require('./alertTemplate'),

	className: 'display-table modal-wrap',

	events: {
		'click .ok': 'ok'
	},

	ok: function() {
		application.modal.empty();
	}
});
