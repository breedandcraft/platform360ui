module.exports = {
	date: {
		type: 'Date'
	},
	textarea: {
		type: 'TextArea'
	},
	text: {
		type: 'Text'
	},
	number: {
		type: 'Number'
	},
	Format: {
		type: 'Radio'
	},
	videoAsset: { 
		type: 'Select',
		options: [{ val: '', label: '[No Video Selected]'}],
		validators: [ 
			{ 
				type: 'required',
				message: 'A video must be selected.'
			} 
		]
	},
	ogvVideoAsset: { 
		type: 'Select',
		options: [{ val: '', label: '[No Video Selected]'}],
		validators: [ 
			{ 
				type: 'required',
				message: 'A video must be selected.'
			} 
		]
	},
	imageAsset: {
		type: 'Select',
		options: [{ val: '', label: '[No Image Selected]'}],
		validators: [ 
			{
				type: 'required',
				message: 'A image must be selected.'
			}
		],
	},
	switch: {
		type: 'Radio',
		options: {
			1: 'On',
			0: 'Off'
		}
	},
	pixel: {
		type: 'Pixel',
		help: 'A pixel for this event is automatically generated. Please input third-party pixels below: individual pixels should be placed on a new line.'
	},
	manualTrack360Pixel: {
		type: 'Text',
		validators: [
			{
				type: 'required',
				message: 'Required. If no Pixel exists, use \'<strong>N/A</strong>\' as the value.'
			},
			{
				type: 'custom ID',
				message: 'Invalid ID provided. If no Pixel exists, use \'<strong>N/A</strong>\' as the value.'
			}
		]
	},
	clickTag: {
		type: 'Text',
		validators: [
			{
				type: 'required',
				message:  'Required.'

			},
			{
				type: 'url',
				message: 'Must be a valid URL.'
			}
		]
	},
	timeTracker: {
		type: 'Radio',
		options: {
			1: 'On',
			0: 'Off'
		}
	}
};
