export default {
	datepicker: {
		type: 'DatePicker'
	},
	date: {
		type: 'Date'
	},
	textarea: {
		type: 'TextArea'
	},
	text: {
		type: 'Text'
	},
	number: {
		type: 'Number'
	},
	radio: {
		type: 'Radio'
	},
	Format: {
		type: 'Radio'
	},
	videoAsset: { 
		type: 'Select',
	},
	ogvVideoAsset: { 
		type: 'Select'
	},
	imageAsset: {
		type: 'Select'
	},
	pixel: {
		type: 'Pixel'
	},
	manualTrack360Pixel: {
		type: 'Text'
	},
	clickTag: {
		type: 'Text',
		validators: [
			{
				type: 'url',
				message: 'Must be a valid URL.'
			}
		]
	},
	switch: {
		type: 'Radio',
		options: {
			1: 'On',
			0: 'Off'
		}
	},
	timeTracker: {
		type: 'Radio',
		options: {
			1: 'On',
			0: 'Off'
		}
	}
};
