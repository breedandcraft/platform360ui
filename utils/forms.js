var forms = {
    error_instruction: "They have been highlighted in red below. Please do something amend them and then attempt to submit the form again.",

	text_inputs: function(container, form_data, errors) {
		$('input[type="text"]',container).each(function() {
	        var input_key   = $(this).attr('name'),
	            input_value = $(this).val(),
	            data_type   = $(this).attr('data-type'),
	            ignore      = $(this).attr('data-ignore') === "true" ? true : false,
                max         = $(this).attr('data-max'),
                required    = get_required($(this).attr('data-required'));

	        // If we have an data-ignore flag, then return out of this iteration
	        // of the loop
	        if (ignore) {
	            return ignore;
	        }

	        // If the input_key is flight_dates then we want to split it up into
	        // flight_start and flight_end
	        if (input_key === 'flight_dates') {
	            // Need to do stuff...
	            var flight_dates = input_value.split(' - '),
	                flight_start = flight_dates[0],
	                flight_end   = flight_dates[1]

	            // Add the dates to our form_data object
	            form_data.flight_start = flight_start
	            form_data.flight_end = flight_end

	            // Check if the dates are valid.
	            if (!moment(flight_start, 'DD MMM YYYY HH:mm', true).isValid() || !moment(flight_end, 'DD MMM YYYY HH:mm', true).isValid()) {
	                errors.push({
	                    'key' : input_key,
	                    'type' : 'invalid'
	                })
	            }
	        } else {
	            // Add the input name as key and value as value to the form_data object
	            form_data[input_key] = input_value
	        }

            //
            // Error Checking
            //

	        // Check if the input_value has any data
	        if(input_value === '' && required) {
	            errors.push({
	                'key' : input_key,
	                'type' : 'empty'
	            })
	        }

	        // Check if the data_type matches the data we got
	        if (data_type === 'number' && isNaN(Number(input_value))) {
	            errors.push({
	                'key' : input_key,
	                'type' : 'type'
	            })
	        }

            // Check if there is a maximum value that we will accept
            // We do a parseInt() on the values because there was a bug
            // previously when we didn't force the types causing the logic to
            // incorrectly return.
            if (max != null && parseInt(input_value) > parseInt(max)) {
                errors.push({
                    'key' : input_key,
                    'type' : 'max'
                })
            }

	    })
	},

	textarea_inputs: function(container, form_data, errors) {
		$('textarea',container).each(function() {
            var input_key   = $(this).attr('name'),
                input_value = $(this).val(),
                data_type   = $(this).attr('data-type'),
                ignore      = $(this).attr('data-ignore') === "true" ? true : false,
                required    = get_required($(this).attr('data-required'));

            // If we have an data-ignore flag, then return out of this iteration
            // of the loop
            if (ignore) {
                return ignore;
            }
            
            form_data[input_key] = input_value

            //
            // Error Checking
            //

            // Check if the input_value has any data
            if(input_value === '' && required) {
                errors.push({
                    'key' : input_key,
                    'type' : 'empty'
                })
            }

            // Check if the data_type matches the data we got
            if (data_type === 'number' && isNaN(Number(input_value))) {
                errors.push({
                    'key' : input_key,
                    'type' : 'type'
                })
            }
        })
	},

	checkbox_inputs: function(container, form_data, errors) {
		$('.checkbox_container',container).each(function() {
            var input_key    = $(this).attr('data-key'),
                ignore       = $(this).attr('data-ignore') === "true" ? true : false,
                required     = get_required($(this).attr('data-required')),
                required_num = $(this).attr('data-required_num') || 1;

            // If we have an data-ignore flag, then return out of this iteration
            // of the loop
            if (ignore) {
                return ignore;
            }

            // Lopp through the checkboxes that are checked
            var checked_checkboxes = []
            $('input[type="checkbox"]:checked', this).each(function() {
                // Add the value to the array
                checked_checkboxes.push($(this).val())
            })

            // Create an array using the data-key of the checkbox_container
            // for the checked_checkboxes to be store in
            form_data[input_key] = checked_checkboxes

            //
            // Error Checking
            //

            // Check if n checked boxes are required
            if (required && checked_checkboxes.length < required_num) {
                errors.push({
                    'key' : input_key,
                    'type' : 'required'
                })
            }
        })
	},

	radio_inputs: function(container, form_data, errors) {
        $('.radio_container',this.$el).each(function() {
            var input_key   = $(this).attr('data-key'),
                input_value = $('input[type="radio"][name="' + input_key + '"]:checked', this).val(),
                required    = get_required($(this).attr('data-required'));

            // Add the input name as key and value as value to the form_data object
            form_data[input_key] = input_value

            //
            // Error Checking
            //

            // Check if a value is required
            if ((input_value === '' || input_value == null) && required) {
                errors.push({
                    'key' : input_key,
                    'type' : 'required'
                })
            }
        })
	},

	clear_input_errors: function(evt) {
        var input_key;
        if (evt.target.type === 'checkbox' || evt.target.type === "radio") {
            input_key = $(evt.target).parent().attr('data-key')
        } else {
            input_key = $(evt.target).attr('name')
        }

        // Remove the red coloring from the label.
        $('#label_' + input_key).removeClass('warning')

        // Cycle through error containers for this key and hiding them
        $('.error_' + input_key).each(function() {
            var element = $(this)

            if (element.is(':visible')) {
                element.hide()
            }
        })

        // Update the error dialogue box/message
        // Not 100% sure why 'forms' works, perhaps because it is being called from
        // the scope of the view?
        forms.update_error_dialogue()
    },

    show_error_dialogue: function(error_count) {
        var error_message = error_count === 1 ? error_count + ' Error Detected.' : error_count + ' Errors Detected.';

        // This shoudn't ever be the case, but if error_count is 0, then don't
        // show the error message.
        if (error_count > 0) {
            application.vent.trigger(
                "showUserMessage",
                {
                    scrollTop: false,
                    type: "stop",
                    message: error_message + " " + this.error_instruction
                }
            );
        }
    },

    update_error_dialogue: function() {
        // We want to update the error count in the error dialogue as we go
        // We're going to use labels with the class warning as our error indicator
        // since there is a possibility 
        var error_count   = $('label.warning',this.$el).length,
            error_message = error_count === 1 ? error_count + ' Error Remaining.' : error_count + ' Errors Remaining.';

        if (error_count === 0) {
            application.vent.trigger("clearUserMessage");
        } else {
            application.vent.trigger(
                "showUserMessage",
                {
                    scrollTop: false,
                    type: "stop",
                    message: error_message + " " + this.error_instruction
                }
            );
        }
    }
};

function get_required(required) {
    if (required === "true") {
        return true;
    } else if (required === "video_only") {
        var is_video = ($( "input:radio[name=type]:checked" ).val() === "video");
	return is_video;
    } else if (required === "banner_only") {
        var is_banner = ($( "input:radio[name=type]:checked" ).val() === "banner");
	return is_banner;
    } else {
        return false;
    }
}

module.exports = forms;
