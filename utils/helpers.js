// Helper to allow us to run if logic beyond the basic Handlebars scope
Handlebars.registerHelper('if_logic', function (var_one, condition, var_two, options) {
	switch (condition) {
		case '==':
			return (var_one == var_two) ? options.fn(this) : options.inverse(this);
		case '===':
			return (var_one === var_two) ? options.fn(this) : options.inverse(this);
		case '<':
			return (var_one < var_two) ? options.fn(this) : options.inverse(this);
		case '<=':
			return (var_one <= var_two) ? options.fn(this) : options.inverse(this);
		case '>':
			return (var_one > var_two) ? options.fn(this) : options.inverse(this);
		case '>=':
			return (var_one >= var_two) ? options.fn(this) : options.inverse(this);
		case '&&':
			return (var_one && var_two) ? options.fn(this) : options.inverse(this);
		case '||':
			return (var_one || var_two) ? options.fn(this) : options.inverse(this);
		case '!=':
			return (var_one != var_two) ? options.fn(this) : options.inverse(this);
		default:
			return options.inverse(this);
	}
});

// Simple maths helper for two value operations
Handlebars.registerHelper('maths', function (first, operator, second, decimal_places, trailing_zeros) {
	// Make sure we're dealing with numbers
	first = Number(first)
	second = Number(second)

	// If we don't have 'decimal_places' set, return a whole number.
	if (!decimal_places) {
		decimal_places = 0
	} else {
		// But if we do have a 'decimal_places' var, we want to make sure it's a
		// number
		decimal_places = Number(decimal_places)
	}

	var value
	if (operator === "+") {
		value = first + second
	} else if (operator === "-") {
		value = first - second
	} else if (operator === "*") {
		value = first * second
	} else if (operator === "/") {
		value = first / second
	} else if (operator === "%") {
		value = ((first / second) * 100)
	}

	// Check if we have a valid number, if not, set value to 0 to prevent visual
	// errors in presentation
    if (isNaN(value) || !isFinite(value)) {
        value = 0
    }

	if (trailing_zeros) {
		return value.toFixed(decimal_places)
	} else {
		// Use Number() to chop off any trailing zeroes
		return Number(value.toFixed(decimal_places))
	}
});

// Add commas to long numbers to improve readability.
Handlebars.registerHelper("number_format", function(value) {
	if (value) {
		return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}
});

// Add commas to long numbers to improve readability, maintain 2 decimal places.
Handlebars.registerHelper("currency_format", function(value) {
	return parseFloat(value).toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
});

// Shorten long/large numbers into a more readable/human friendly format
Handlebars.registerHelper('long_number_shortener', function(value) {
	// Let's make sure that we have a number to work with.
	value = Number(value)

	if (value < 1000) {
		// If the number is less than 1000 we don't really need to do anything
		// with it, so we just return it (after rounding it to 2 decimal places)
		return Number(value.toFixed(2))
	} else if (value < 999949) {
		// This seems like an odd number, but this is to ensure numbers like 999999
		// aren't returned as 1000k

		// Divide the value by 1000 (one-thousand) and round to 1 decimal place
		value = Number((value / 1000).toFixed(1)) + 'k'
	} else {
		// Divide the value by 1000000 (one-million) and round to 2 decimal places
		value = Number((value / 1000000).toFixed(2)) + 'm'
	}

	return value
});

// Returns a count of active line items when passed an array/collection of line items
Handlebars.registerHelper('active_line_item_count', function(line_items) {
	var count = 0

	// Check that the object is not empty since the forEach will error out on
	// an empty object.
	if (line_items.length > 0) {
		line_items.forEach(function(line_item) {
			if (moment().isBetween(line_item.flight_start, line_item.flight_end) && line_item.status === 1) {
				count++;
			}
		})
	}	

	return count
});

// Returns how far from now the specified date/time is.
Handlebars.registerHelper("from_now", function(date) {
	return moment(date).fromNow(true)
});

// Returns the time passed to it with the formatting passed to it
Handlebars.registerHelper("date_formatter", function(time, format) {
	return moment(time).format(format)
});

// Returns a percentage value as a representation of time remaining in a range
// between two dates
Handlebars.registerHelper("time_percentage_remaining_in_range", function(start, end, now) {
	start = moment(start).unix()
	end = moment(end).unix()
	if (!now) {
		var now = moment().unix()
	} else {
		now = moment(now).unix()
	}
	var duration = end - start
	var elapsed = now - start

	return (100 - ((elapsed / duration) * 100)).toFixed(2)
});

// Checks if now is between two specified dates/times
Handlebars.registerHelper('is_date_in_range', function(start, end) {
	if (moment().isBetween(start, end)) {
		return true
	} else if (moment().isAfter(end)) {
		return 'after'
	} else {
		return 'before'
	}
});

// Returns an 's' if the value it is passed is not 1.
Handlebars.registerHelper("plural_check", function(value) {
	if (value === 1) {
		return
	} else {
		return 's'
	}
});

// Change an array into a single string, joining with an optional "thing".
Handlebars.registerHelper("array_join", function(array, joiner) {
	return array.join(joiner)
});