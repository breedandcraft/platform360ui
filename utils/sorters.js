var sorters = {
	sorted_column: null,
	sort_cta: null,
	sort_collection: function(evt) {
		evt.preventDefault();

		var key = $(evt.target).attr('data-key'),
			type = $(evt.target).attr('data-type'),
			direction = $(evt.target).attr('data-direction'),
			sorter;

		// The sorting function is slightly different based on the type of data
		// and desired sorting order, so we check what we have and want to do...
		if (type === 'string') {
			if (direction === 'asc') {
				sorter = function(item_one, item_two) {
		            return [this.prepare_string(item_one.get(key)).localeCompare(this.prepare_string(item_two.get(key)))]
		        }
			} else {
				sorter = function(item_one, item_two) {
		            return [this.prepare_string(item_two.get(key)).localeCompare(this.prepare_string(item_one.get(key)))]
		        }
			}
		} else if (type === 'date') {
			if (direction === 'asc') {
				sorter = function(item_one, item_two) {
		            return [this.prepare_date(item_one.get(key)) - this.prepare_date(item_two.get(key))]
		        }
			} else {
				sorter = function(item_one, item_two) {
		            return [this.prepare_date(item_two.get(key)) - this.prepare_date(item_one.get(key))]
		        }
			}
		} else if (type === 'int') {
			if (direction === 'asc') {
				sorter = function(item_one, item_two) {
		            return [this.prepare_int(item_one.get(key)) - this.prepare_int(item_two.get(key))]
		        }
			} else {
				sorter = function(item_one, item_two) {
		            return [this.prepare_int(item_two.get(key)) - this.prepare_int(item_one.get(key))]
		        }
			}
		}

		// Make the function we want the comparator for the collection.
		this.collection.comparator = sorter

		// And finally apply the sort...
		// If the view has 'reorderOnSort: true' then this will only sort, not 
		// re-render the entire view, which is nice.
		this.collection.sort()
	},

	sort_table_styling: function(evt) {
		// get the column index
		var colIndex = parseInt($(evt.target).attr('data-col'));
		//get the specific column in the colgroup
		var columns = $('colgroup col', $(evt.target).parents('table')[0]);

		//reset active styling
		if (this.sorted_column != null) {
			$(columns[this.sorted_column]).attr('style', '');
		}

		if (this.sort_cta) {
			$(this.sort_cta).removeClass('text-clr');
		}

		// set active styling
		$(evt.target).addClass('text-clr');
		$(columns[colIndex]).css('background', 'rgba(0, 108, 200, 0.1)');

		// cache active elements
		this.sorted_column = colIndex;
		this.sort_cta = evt.target;
	},

	// To account for circumstances where the data is empty (null or no value),
	// we still want to sort those correctly (at the bottom of the sort), so we
	// use these functions.
	prepare_string: function(value) {
		if (value == null || value == 0 ) {
            return ''
        } else {
            return value.toLowerCase()
        }
	},

	prepare_date: function(value) {
		if (value == null || value == 0 ) {
            return -1
        } else {
        	// Transform the date into a unix timestamp so that it can be easily
        	// sorted.
            return parseInt(Date.parse(value).toString())
        }
	},

	prepare_int: function(value) {
		if (value == null || value == 0 || isNaN(value)) {
            return -1
        } else {
            return value
        }
	}
}

module.exports = sorters
